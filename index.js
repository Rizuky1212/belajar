const express = require('express');

const app = express();
const router = express.Router();

router.use('/Detail', (req,res,next) => {
    res.json({
        name: "Rizky Langit Ganteng",
        email: "Rizukylangit@gmail.com"
    })
})

app.use('/', router);

app.listen(4000)